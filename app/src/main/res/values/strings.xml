<resources>
    <string name="app_name">"Cisco's Security Features"</string>
    <string name="passwords_and_privileges_commands">Passwords and Privileges Commands</string>
    <string name="contact_cisco">Contact Cisco</string>
    <string name="ip_security_options_commands">IP Security Options Commands</string>
    <string name="secure_shell_commands">Secure Shell Commands</string>
    <string name="go_now">Go Now!</string>
    <string name="article_title">Passwords and Privileges Commands</string>
    <string name="article_subtitle">"Let's learn!"</string>
    <string name="article_text">This chapter describes the commands used to establish password protection and configure privilege levels. Password protection lets you restrict access to a network or a network device. Privilege levels let you define what commands users can issue after they have logged in to a network device.
        \n\n
For information on how to establish password protection or configure privilege levels, refer to the "Configuring Passwords and Privileges" chapter in the Cisco IOS Security Configuration Guide. For configuration examples using the commands in this chapter, refer to the "Passwords and Privileges Configuration Examples" section located at the end of the "Configuring Passwords and Privileges" chapter in the Cisco IOS Security Configuration Guide.
\n\n
        <i>1. enable password</i>
        \n\n
To set a local password to control access to various privilege levels, use the enable password command in global configuration mode. To remove the password requirement, use the no form of this command.
\n\n
enable password [level level] {password | [encryption-type] encrypted-password}
\n\n
no enable password [level level]
    \n\n
    <i>2. enable secret</i>
    \n\n
    To specify an additional layer of security over the enable password command, use the enable secret command in global configuration mode. To turn off the enable secret function, use the no form of this command.
    \n\n
    enable secret [level level] {password | [encryption-type] encrypted-password}
    \n\n
    no enable secret [level level]
    \n\n
       <i>3. password</i>
        \n\n
To specify a password on a line, use the password command in line configuration mode. To remove the password, use the no form of this command.
\n\n
password password
\n\n
no password
        \n\n
        <i>4. privilege</i>
        \n\n
To configure a new privilege level for users and associate commands with that privilege level, use the privilege command in global configuration mode. Use the no form of this command to revert to default privileges for the specified command.
\n\n
privilege mode {level level | reset} command-string
\n\n
no privilege mode {level level | reset} command-string
        \n\n
        <i>5. privilege level</i>
        \n\n
To set the default privilege level for a line, use the privilege level command in line configuration mode. To restore the default user privilege level to the line, use the no form of this command.
\n\n
privilege level level
\n\n
no privilege level
        \n\n
        <i>6. service password-encryption</i>
        \n\n
To encrypt passwords, use the service password-encryption command in global configuration mode. To restore the default, use the no form of this command.
\n\n
service password-encryption
\n\n
no service password-encryption
        \n\n
        <i>7. show privilege</i>
        \n\n
To display your current level of privilege, use the show privilege command in EXEC mode.
\n\n
show privilege
        \n\n
        <i>8. username</i>
        \n\n
To establish a username-based authentication system, use the username command in global configuration mode. Use the no form of this command to remove an established username-based authentication.
\n\n
username name {nopassword | password password | password encryption-type encrypted-password}
\n\n
username name password secret
\n\n
username name [access-class number]
\n\n
username name [autocommand command]
\n\n
username name [callback-dialstring telephone-number]
\n\n
username name [callback-rotary rotary-group-number]
\n\n
username name [callback-line [tty] line-number [ending-line-number]]
\n\n
username name dnis
\n\n
username name [nocallback-verify]
\n\n
username name [noescape] [nohangup]
\n\n
username name [privilege level]
\n\n
username name user-maxlinks number
\n\n
username [lawful-intercept] name [privilege privilege-level | view view-name] password password
\n\n
no username name
        \n\n
    </string>
    <string name="app_name1">Chapter 1</string>
    <string name="app_name2">Chapter 2</string>
    <string name="article_title1">IP Security Options Commands</string>
    <string name="article_subtitle2">Let\'s discover!</string>
    <string name="article_text2">This chapter describes IP Security Options (IPSO) commands. IPSO is generally used to comply with the U.S. government\'s Department of Defense security policy.
\n\n
To find complete descriptions of other commands used when configuring IPSO, refer to the Cisco IOS Command Reference Master Index or search online.
\nn
For IPSO configuration information, refer to the "Configuring IP Security Options" chapter in the
Cisco IOS Security Configuration Guide.
\n\n
<i>1. dnsix-dmdp retries</i>
        \n\n
To set the retransmit count used by the Department of Defense Intelligence Information System Network Security for Information Exchange (DNSIX) Message Delivery Protocol (DMDP), use the dnsix-dmdp retries command in global configuration mode. To restore the default number of retries, use the no form of this command.
\n\n
dnsix-dmdp retries count
\n\n
no dnsix-dmdp retries count
        \n\n
        <i>2. dnsix-nat authorized-redirection</i>
        \n\n
To specify the address of a collection center that is authorized to change the primary and secondary addresses of the host to receive audit messages, use the dnsix-nat authorized-redirection global configuration command. To delete an address, use the no form of this command.
\n\n
dnsix-nat authorized-redirection ip-address
\n\n
no dnsix-nat authorized-redirection ip-address
        \n\n
        <i>3. dnsix-nat primary</i>
        \n\n
To specify the IP address of the host to which Department of Defense Intelligence Information System Network Security for Information Exchange (DNSIX) audit messages are sent, use the dnsix-nat primary command in global configuration mode. To delete an entry, use the no form of this command.
\n\n
dnsix-nat primary ip-address
\n\n
no dnsix-nat primary ip-address
        \n\n
        <i>4. dnsix-nat secondary</i>
        \n\n
To specify an alternate IP address for the host to which Department of Defense Intelligence Information System Network Security for Information Exchange (DNSIX) audit messages are sent, use the dnsix-nat secondary command in global configuration mode. To delete an entry, use the no form of this command.
\n\n
dnsix-nat secondary ip-address
\n\n
no dnsix-nat secondary ip-address
        \n\n
        <i>5. dnsix-nat source</i>
        \n\n
To start the audit-writing module and to define the audit trail source address, use the dnsix-nat source command in global configuration mode. To disable the Department of Defense Intelligence Information System Network Security for Information Exchange (DNSIX) audit trail writing module, use the no form of this command.
\n\n
dnsix-nat source ip-address
\n\n
no dnsix-nat source ip-address
        \n\n
<i>6. dnsix-nat transmit-count</i>
        \n\n
To have the audit writing module collect multiple audit messages in the buffer before sending the messages to a collection center, use the dnsix-nat transmit-count command in global configuration mode. To revert to the default audit message count, use the no form of this command.
\n\n
dnsix-nat transmit-count count
\n\n
no dnsix-nat transmit-count count
        \n\n
        <i>7. ip security aeso</i>
        \n\n
To attach Auxiliary Extended Security Options (AESOs) to an interface, use the ip security aeso command in interface configuration mode. To disable AESO on an interface, use the no form of this command.
\n\n
ip security aeso source compartment-bits
\n\n
no ip security aeso source compartment-bits
        \n\n
        <i>8. ip security dedicated</i>
        \n\n
To set the level of classification and authority on the interface, use the ip security dedicated command in interface configuration mode. To reset the interface to the default classification and authorities, use the no form of this command.
\n\n
ip security dedicated level authority [authority...]
\n\n
no ip security dedicated level authority [authority...]
        \n\n
        <i>9. ip security eso-info</i>
        \n\n
To configure system-wide defaults for extended IP Security Option (IPSO) information, use the ip security eso-info command in global configuration mode. To return to the default settings, use the no form of this command.
\n\n
ip security eso-info source compartment-size default-bit
\n\n
no ip security eso-info source compartment-size default-bit
        \n\n
        <i>10. ip security eso-max</i>
        \n\n
To specify the maximum sensitivity level for an interface, use the ip security eso-max command in interface configuration mode. To return to the default, use the no form of this command.
\n\n
ip security eso-max source compartment-bits
\n\n
no ip security eso-max source compartment-bits
        \n\n
        <i>11. ip security eso-min</i>
        \n\n
To configure the minimum sensitivity for an interface, use the ip security eso-min command in interface configuration mode. To return to the default, use the no form of this command.
\n\n
ip security eso-min source compartment-bits
\n\n
no ip security eso-min source compartment-bits
        \n\n
        <i>12. ip security extended-allowed</i>
        \n\n
To accept packets on an interface that has an extended security option present, use the ip security extended-allowed command in interface configuration mode. To restore the default, use the no form of this command.
\n\n
ip security extended-allowed
\n\n
no ip security extended-allowed
        \n\n
        <i>13. ip security first</i>
        \n\n
To prioritize the presence of security options on a packet, use the ip security first command in interface configuration mode. To prevent packets that include security options from moving to the front of the options field, use the no form of this command.
\n\n
ip security first
\n\n
no ip security first
        \n\n
        <i>14. ip security ignore-authorities</i>
        \n\n
To have the Cisco IOS software ignore the authorities field of all incoming packets, use the ip security ignore-authorities command in interface configuration mode. To disable this function, use the no form of this command.
\n\n
ip security ignore-authorities
\n\n
no ip security ignore-authorities
        \n\n
        <i>15. ip security ignore-cipso</i>i
        \n\n
To enable Cisco IOS software to ignore the Commercial IP Security Option (CIPSO) field of all incoming packets at the interface, use the ip security ignore-cipso command in interface configuration mode. To disable this function, use the no form of this command.
\n\n
ip security ignore-cipso
\n\n
no ip security ignore-cipso
        \n\n
        <i>16. ip security implicit-labelling</i>
        \n\n
To force the Cisco IOS software to accept packets on the interface, even if they do not include a security option, use the ip security implicit-labelling command in interface configuration mode. To require security options, use the no form of this command.
\n\n
ip security implicit-labelling [level authority [authority...]]
\n\n
no ip security implicit-labelling [level authority [authority...]]
        \n\n
        <i>17. ip security multilevel</i>
        \n\n
To set the range of classifications and authorities on an interface, use the ip security multilevel command in interface configuration mode. To remove security classifications and authorities, use the no form of this command.
\n\n
ip security multilevel level1 [authority1...] to level2 authority2 [authority2...]
\n\n
no ip security multilevel
        \n\n
        <i>18. ip security reserved-allowed</i>
        \n\n
To treat as valid any packets that have Reserved1 through Reserved4 security levels, use the ip security reserved-allowed command in interface configuration mode. To disallow packets that have security levels of Reserved3 and Reserved2, use the no form of this command.
\n\n
ip security reserved-allowed
\n\n
no ip security reserved-allowed
        \n\n
        <i>19. ip security strip</i>
        \n\n
To remove any basic security option on outgoing packets on an interface, use the ip security strip command in interface configuration mode. To restore security options, use the no form of this command.
\n\n
ip security strip
\n\n
no ip security strip
        \n\n
        <i>20. show dnsix</i>
        \n\n
To display state information and the current configuration of the DNSIX audit writing module, use the show dnsix command in privileged EXEC mode.
\n\n
show dnsix
        \n\n
    </string>
    <string name="app_name3">Chapter3</string>
    <string name="article_title2">Secure Shell Commands</string>
    <string name="article_subtitle3">Let\'s study!</string>
    <string name="article_text3">This chapter describes Secure Shell (SSH) commands. SSH is an application and a protocol that provides a secure replacement to the Berkeley r-tools. The protocol secures the remote connection to a router using standard cryptographic mechanisms, and the application can be used similarly to the Berkeley rexec and rsh tools. There are currently two versions of SSH available, SSH Version 1 and SSH Version 2. Only SSH Version 1 is implemented in the Cisco IOS software.
\n\n
To find complete descriptions of other commands used when configuring SSH, refer to the Cisco IOS Command Reference Master Index or search online.
\n\n
For SSH configuration information, refer to the "Configuring Secure Shell" chapter in the Cisco IOS Security Configuration Guide.
    \n\n
        <i>1. disconnect ssh</i>
        \n\n
To terminate a Secure Shell (SSH) connection on your router, use the disconnect ssh privileged EXEC command.
\n\n
disconnect ssh [vty] session-id
        \n\n
        <i>2. ip ssh</i>
        \n\n
To configure Secure Shell (SSH) control parameters on your router, use the ip ssh global configuration command. To restore the default value, use the no form of this command.
\n\n
ip ssh {[timeout seconds]} | [authentication-retries integer]}
\n\n
no ip ssh {[timeout seconds]} | [authentication-retries integer]}
        \n\n
        <i>3. show ip ssh</i>
        \n\n
To display the version and configuration data for Secure Shell (SSH), use the show ip ssh privileged EXEC command.
\n\n
show ip ssh
        \n\n
        <i>4. show ssh</i>
        \n\n
To display the status of Secure Shell (SSH) server connections, use the show ssh privileged EXEC command.
\n\n
show ssh
        \n\n
        <i>5. ssh</i>
        \n\n
To start an encrypted session with a remote networking device, use the ssh user EXEC command.
\n\n
ssh [-l userid] [-c {des | 3des}] [-o numberofpasswdprompts n] [-p portnum] {ipaddr | hostname} [command]
        \n\n
    </string>
    <string name="app_name4">Chapter 4</string>
    <string name="article_title3">Contact Cisco</string>
    <string name="article_subtitle4">Tell us about your issue so we can get you to the right people, as soon as possible.</string>
    <string name="article_text4">
        <b>Contact Us</b>
        \n\n
Customers and Partners :
1-800-553-6387
        \n\n
Technical Support :
1-800-553-2447 or 1-408-526-7209
        \n\n
        <b>Cisco Systems, Inc.</b>
        \n\n
Corporate Headquarters
        \n\n
170 West Tasman Dr.
        \n\n
San Jose, CA 95134
        \n\n
USA
    \n\n</string>
    <string name="app_name5">Further Information</string>
    <string name="further_information">Further Information</string>
</resources>
